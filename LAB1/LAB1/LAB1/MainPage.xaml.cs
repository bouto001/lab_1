﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace LAB1
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public Xamarin.Forms.Color ColorText { get; set; } = Xamarin.Forms.Color.Black;
        public MainPage()
        {
            InitializeComponent();
            //Label label = new Label
            //{
            //    Text = "This is the text",
            //    TextColor = ColorText
            //};

            //label
            BindingContext = this;
        }


        private void Button_Clicked(object sender, EventArgs e)
        {
            switch (((Button)sender).Text)
            {
                case "Red":
                    ColorText = Xamarin.Forms.Color.Red;
                    break;
                case "Blue":
                    ColorText = Xamarin.Forms.Color.Blue;
                    break;
                case "Green":
                    ColorText = Xamarin.Forms.Color.Green;
                    break;
                case "Yellow":
                    ColorText = Xamarin.Forms.Color.Yellow  ;
                    break;
            }

            OnPropertyChanged(nameof(ColorText));
        }
    }
}
